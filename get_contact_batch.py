#!/usr/bin/python -u
# -*- coding: utf-8 -*-
# -u unbuffered output
#
#   get_contact_batch.py
#
#   PYTHON 3 VERSION
#
#   Simple SOAP call to Oracle Sales Cloud to retrieve contacts updated since a given date.
#   Uses the ADF Service: Trading Community Person Information (11.1.9.0.0)
#   Service Path: https://<crm_server:PortNumber>/foundationParties/PersonService?WSDL
#
#   Example:
#   python get_contact_batch.py -d --fetch-start=0 --fetch-size=5 --since 2015-05-27T14:19:50.0Z      
#       -d debug, fetches batch of 5 records, starting at record 0 since the given timestamp
#
#   Martin Bridge, Oracle Corporation, Oct 2015
#
#   06  03-nov-2014 mbridge     Adapted from general SOAP script
#   07  01-oct-2015 mbridge     Sales Cloud findContact
#   08  01-dec-2015 mbridge     Save as Python 3 version
#   09  02-dec-2015 mbridge     Fixed unicode problems on windows (PYTHONIOENCODING=utf-8)
#   10  08-jan-2016 mbridge     Connection parameters moved to config.py
#
import argparse
import sys
import os
import httplib2
import base64
from xml.dom.minidom import parseString
from time import time
from config import *

debug=False

# SOAP Message Template
# Tokens:
#   1   fetchStart
#   2   fetchSize
#   3   LastUpdateDate
#   4   Name (Testing only, remove later)
API_URL = "/foundationParties/PersonService"
SM_TEMPLATE  = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://xmlns.oracle.com/apps/cdm/foundation/parties/personService/applicationModule/types/" xmlns:typ1="http://xmlns.oracle.com/adf/svc/types/">
   <soapenv:Header/>
   <soapenv:Body>
      <typ:findPerson>
         <typ:findCriteria>
            <typ1:fetchStart>%s</typ1:fetchStart>
            <typ1:fetchSize>%s</typ1:fetchSize>
            <!--Optional:-->
            <typ1:filter>
               <!--Optional:-->
               <typ1:conjunction>And</typ1:conjunction>
               <!--1 or more repetitions:-->
               <typ1:group>
                  <!--Optional:-->
                  <typ1:conjunction>And</typ1:conjunction>
                  <typ1:upperCaseCompare>false</typ1:upperCaseCompare>
                  <!--1 or more repetitions:-->
                  <typ1:item>
                     <!--Optional:-->
                     <typ1:conjunction>And</typ1:conjunction>
                     <typ1:upperCaseCompare>false</typ1:upperCaseCompare>
                     <typ1:attribute>LastUpdateDate</typ1:attribute>
                     <typ1:operator>AFTER</typ1:operator>
                     <typ1:value>%s</typ1:value>
                  </typ1:item>
                  <!-- Name filter here if required -->
                  %s
               </typ1:group>
               <!--Zero or more repetitions:-->
               <typ1:nested/>
            </typ1:filter>
            <!--Optional:-->
            <typ1:sortOrder>
               <!--1 or more repetitions:-->
               <typ1:sortAttribute>
                  <typ1:name>LastUpdateDate</typ1:name>
                  <typ1:descending>false</typ1:descending>
               </typ1:sortAttribute>
            </typ1:sortOrder>
            <!-- Add findAttribute tags here to control which fields are returned -->
            <!-- Big performance boost by limiting fields returned -->
            <!-- Remove these entries to return ALL fields -->
            <typ1:findAttribute>PartyId</typ1:findAttribute>
            <typ1:findAttribute>PersonLastName</typ1:findAttribute>
            <typ1:findAttribute>PersonFirstName</typ1:findAttribute>
            <typ1:findAttribute>EmailAddress</typ1:findAttribute>
            <typ1:findAttribute>LastUpdateDate</typ1:findAttribute>
          </typ:findCriteria>
      </typ:findPerson>
   </soapenv:Body>
</soapenv:Envelope>
"""

# Name filter for convenience of testing by filtering on LastName
NAME_FILTER = """
<typ1:item>
    <!--Optional:-->
    <typ1:conjunction>And</typ1:conjunction>
    <typ1:upperCaseCompare>false</typ1:upperCaseCompare>
    <typ1:attribute>PersonLastName</typ1:attribute>
    <typ1:operator>STARTSWITH</typ1:operator>
    <typ1:value>%s</typ1:value>
</typ1:item>
"""


#=========================================================================================
# Initialise - setup data & command line args
# Command line args
# -d   debug
# --fetch-start  Start record in SOAP call
# --fetch-size   Number of records to fetch in SOAP call
# --since        Timestamp to fetch records: records modified AFTER  this time are fetched
#
def init():
    global args
    global debug
    global SM_TEMPLATE

    parser = argparse.ArgumentParser(description='Extract Contacts from Sales Cloud')

    parser.add_argument('-d', '--debug', action='store_true', dest='debug', help='debug' )

    parser.add_argument('--fetch-start', type=int, action='store', default=0,
                        metavar=('First record'),
                        help='Start record number (used to page results)')

    parser.add_argument('--fetch-size', type=int, action='store', default=5,
                        metavar='# records to fetch', 
                        help='Number of records to fetch')

    parser.add_argument('--since', action='store', default='1970-01-01T00:00:00.0Z',
                        metavar='Timestamp', 
                        help='Only fetch records modified since this timestamp.  '
                            +'Timestamp in ISO 8601 format, e.g. 2015-10-21T07:28:00.0Z')
    
    args = parser.parse_args()

    debug = args.debug
    
    # Need to set python specific encoding on Windows as cmd.exe and other things 
    # don't work too well without it for certain unicode characters (such as chinese)
    if (sys.platform == "win32"):
        if (os.environ.get("PYTHONIOENCODING", "Not Set") == "Not Set"):
            print("You must set the following environment variable on windows for correct handling of Unicode")
            print("")
            print("set PYTHONIOENCODING=utf-8")
            sys.exit()
        

#=========================================================================================
# Info messages to stderr
def info_msg(msg):
    print(msg, file=sys.stderr)

#=========================================================================================
# Make the SOAP call
# returns response + payload
def call_ws(fetch_start, fetch_size, since, name):

    # Add name to name filter.  
    # If no name supplied, do not add the filter to the SOAP request  
    if (name != ""):
        name_filter = NAME_FILTER%(name)
    else:
        name_filter =""
    
    soap_msg = SM_TEMPLATE%(fetch_start, fetch_size, since, name_filter)
 
    if (debug):
        info_msg("================================================================================")
        info_msg("Sending %d bytes" % len(soap_msg))
        # dump results to file
        f = open("req.xml", "w")
        f.write(soap_msg)
        f.close()

    # encode/decode to handle python 3 byte/str mis-match 
    auth = base64.b64encode(bytes(USERNAME+":"+PASSWORD, "UTF-8")).decode("UTF-8")
    header = {"Content-Type": "text/xml;charset=utf-8"
              ,"Authorization": "Basic " + auth
#                ,"SOAPAction": "http://xmlns.oracle.com/pps/cdm/foundation/parties/personService/applicationModule/findPerson"
#                ,"Content-Length": str(len(soap_msg))
#                ,"Host": HOST
              }
    
    h = httplib2.Http()
    response,payload = h.request("https://" + HOST + API_URL, "POST", soap_msg, headers = header )
    
    payloadstr = payload.decode("utf8")
    
    if (debug):
        info_msg("HTTP Status: %d" % response.status)
        # dump raw results to file
        f = open("resp.xml", "wb")
        f.write(payload)
        f.close()

    return response, payloadstr

#=========================================================================================
# Need to handle the fact that some tags are blank.  If they are, the node returns None and 
# we don't get a value
def get_node_value(xmldoc, tagname):
    mynode = xmldoc.getElementsByTagName(tagname)[0].firstChild
    if (mynode != None):
        return mynode.nodeValue 
    else:
        return ""

#=========================================================================================
# get batch of contacts, return most recent timestamp
def get_contacts(fetch_start, fetch_size, since, name):

    starttime = time()
    response, payload = call_ws(fetch_start, fetch_size, since, name)
    timetaken = time() - starttime
    info_msg("Time: %f" % timetaken)

    # Parse result
    # WARNING: Namespace tags <ns2:... hard coded - may need to change, depending 
    # on the SOAP response.
    #
    xmldom = parseString(payload)
    contacts = xmldom.getElementsByTagName("ns2:Value")    

    most_recent=""
    
    nrecs=0
    for contact in contacts:
        nrecs = nrecs + 1
        
        partyid = get_node_value(contact, "ns2:PartyId")
        lastname = get_node_value(contact, "ns2:PersonLastName")
        firstname = get_node_value(contact, "ns2:PersonFirstName")
        email = get_node_value(contact, "ns2:EmailAddress")
        updated = get_node_value(contact, "ns2:LastUpdateDate")
    
        # Formatted output of record
        # Replace with your own format here
        try:
            print(partyid,
                  lastname,
                  firstname,
                  email,
                  updated,
                  sep="|"
                 )
            # Keep track of most recent record retrieved
            if (updated > most_recent):
                most_recent = updated
                
        except UnicodeEncodeError as err:
            print("**UnicodeEncodeError**")
            print(err)
            
    return(nrecs, most_recent)
    
#=========================================================================================
def main():
    init()
    
    info_msg("Sales Cloud SOAP Fetch ")
    info_msg(("fetch-start=%s, fetch-size=%s, since=%s") 
          % (args.fetch_start, args.fetch_size, args.since))
    
    nrecs, most_recent = get_contacts(args.fetch_start, args.fetch_size, args.since, "")

    # Need most recent for next run
    info_msg("\nRecords fetched:       " + str(nrecs))
    info_msg("Most recent timestamp: " + most_recent)

#=========================================================================================
if __name__ == "__main__":
    main()

#--
