# Sales Cloud Extract #

Python program for perform an extract of contacts from Oracle Sales Cloud using a quick 
and dirty web service call.  Consists of two python 3 scripts (plus a config file - see Installation section):

 * get_contact_batch.py
 * get_all_recent_contacts.py
    
#### get_contact_batch.py ####

Get one set of records from sales cloud by specifying a timestamp, and batch size.  

Example: debug mode (-d), fetch 20 records, starting at record 0 that have changed since 
the given date.
    
    python get_contact_batch.py -d --fetch-start=0 --fetch-size=5 --since 2015-05-27T14:19:50.0Z    
    
For help:

    python get_contact_batch.py -h
    
#### get_all_recent_contacts.py ####
This is a handler to call the other script.  It just loops round calling get_contact_batch 
until all records are fetched.
    
It also saves the timestamp of the most recent record so that the job can be restarted 
any time to get any new records since the last run.
    
Batch size (which is a parameter to the previous script) is hard coded in this script.

    BATCHSIZE=500

### Installation / Getting Started ###

#### config.py ####

Firstly, create the config.py file by copying config-sample.py and changing the connection details to match your service.

Connection information is edited in this file as follows:
    
    HOST = "<Your Hostname here>:443";
    USERNAME = "<Your Username here>";
    PASSWORD = "<Your password here>";

#### Dependencies ####

 * Written for Python 3
 * Requires *httplib2* python library, can be installed using pip:
 
 
    pip install httplib2
    
#### Useful Information ####

The following blog entry by Richard Bingham is useful in explaining how to call the Find 
method of the Sales Cloud web services

https://blogs.oracle.com/fadevrel/entry/explaining_the_use_of_the

#### Modifying and Extending ####

This script can be used against any web service.  For Sales Cloud, use 
https://fusionappsoer.oracle.com/oer/ and search for the appropriate ADF Service.  
You'll then need to get the example SOAP payload and use that in this script.

* Martin Bridge, Oracle, Oct 2015
