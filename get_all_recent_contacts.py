#!/usr/bin/python
#  -*- coding: utf-8 -*-
#
# batch calling harness
#
# Repeatedly call get_contact_batch.py to get batches of data of a given size
# Timestamp since of most recent fetchd record is saved in a text file, so 
# that the next run of this program will only pick up changes since the last run.
#
# Martin Bridge, Oracle Corporation, Oct 2015
#
#   01  06-oct-2015 mbridge     Created
#   02  02-dec-2015 mbridge     Use info_msg simple logging from get_contact_batch
#

import get_contact_batch
from get_contact_batch import info_msg
import os.path
    
# Timestamp file
TFILE="lasttime.txt"
TIMEZERO="1970-01-01T00:00:00"
BATCHSIZE=500

if __name__ == '__main__':

    lastruntime=""
    # Get last timestamp from file
    if(os.path.isfile(TFILE)): 
        f = open(TFILE, "r")
        lastruntime = f.readline()
        f.close()
    else:
        lastruntime=TIMEZERO

    info_msg("Last run time = " + lastruntime)

    # Loop round, getting one batch of records until none returned.
    nrecs=1
    recnum = 0
    mostrecentbatch = lastruntime
    while (nrecs != 0):
        nrecs, newtime = get_contact_batch.get_contacts(recnum, BATCHSIZE, lastruntime, "")
        recnum = recnum + BATCHSIZE
#        info_msg("This batch time = " + newtime) 
        if (newtime != "" and newtime > mostrecentbatch):
            mostrecentbatch = newtime
            
        info_msg("Most recent batch = " + mostrecentbatch)
    
    # Could write to file each batch, or once at the end        
    if (mostrecentbatch > lastruntime):
        # Save timestamp
        f = open(TFILE, "w")
        f.write(mostrecentbatch)
        f.close()
